package com.darkempire.lab.systemanalysis;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by siredvin on 30.03.15.
 *
 * @author siredvin
 */
public class Main extends Application{
    private static final String fileSeparator = System.getProperty("file.separator");
    //private static List<Class> loadedClass = new ArrayList<>();
    public static final NumberFormat numberFormat = NumberFormat.getNumberInstance();
    public static final StringConverter<Double> stringConverter = new StringConverter<Double>() {
        @Override public String toString(Double value) {
            // If the specified value is null, return a zero-length String
            if (value == null) {
                return "";
            }
            return numberFormat.format(value);
        }

        @Override public Double fromString(String value) {
            try {
                // If the specified value is null or zero-length, return null
                if (value == null) {
                    return null;
                }
                value = value.trim();
                if (value.length() < 1) {
                    return null;
                }
                // Perform the requested parsing
                return numberFormat.parse(value).doubleValue();
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
        }
    };
    static{
        numberFormat.setMaximumFractionDigits(20);
    }

    public static SpinnerValueFactory<Double> createValueFactory(double min,double max,double start,double step){
        SpinnerValueFactory<Double> svf = new SpinnerValueFactory.DoubleSpinnerValueFactory(min,max,start,step);
        svf.setConverter(stringConverter);
        return svf;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        SettingClass.loadSetting();
        checkMathamaticaLink(primaryStage);
        FXMLLoader loader = new FXMLLoader();
        primaryStage.getIcons().addAll(new Image(Main.class.getResource("res/ico/clipboard2.png").toExternalForm()));
        primaryStage.setIconified(true);
        loader.setLocation(Main.class.getResource("res/main.fxml"));
        primaryStage.setScene(new Scene(loader.load()));
        Controller controller = loader.getController();
        controller.init(primaryStage);
        primaryStage.setTitle("Сьома лабораторна робота");
        primaryStage.show();
    }

    private void checkMathamaticaLink(Stage primaryStage) throws MalformedURLException, ClassNotFoundException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try {
            classLoader.loadClass("com.wolfram.jlink.KernelLink");
        } catch (ClassNotFoundException e){
            File result = trySearch();
            if (result==null) {
                MonologGeneratorPane.showAcceptDialog("Математика не знайдена", "Зараз вам доведеться вказати шлях до математики вручну");
                DirectoryChooser chooser = new DirectoryChooser();
                File f = chooser.showDialog(primaryStage);
                if (f == null) {
                    MonologGeneratorPane.showErrorDialog("Немає математики", "Але, на жаль, вона необхідна для роботи програми. Програма зараз автоматично завершиться");
                    System.exit(20);
                }
                result = isMathematicaFolder(f);
            }
            if (result==null){
                MonologGeneratorPane.showErrorDialog("Немає математики","Але, на жаль, вона необхідна для роботи програми. Програма зараз автоматично завершиться");
                System.exit(20);
            }
            SettingClass.MATHEMATICA_PATH = result.getParentFile().getParentFile().getParentFile().getParentFile().getAbsolutePath();
            SettingClass.saveSetting();
            URLClassLoader sysloader = (URLClassLoader)ClassLoader.getSystemClassLoader();
            Class<?> sysclass = URLClassLoader.class;
            try {
                Method method = sysclass.getDeclaredMethod("addURL", URL.class);
                method.setAccessible(true);
                URL jLinkURL = result.toURI().toURL();
                method.invoke(sysloader, jLinkURL);
            } catch (Throwable t) {
                Log.error(Log.debugIndex,t);
            }
        }
    }

    public static void main(String[] args) {
        Main.launch(args);
    }

    public static File isMathematicaFolder(File f){
        if (!Files.exists(f.toPath())) {
            Log.log(Log.debugIndex,f.toString(),"not exists");
            return null;
        }
        File jLink = new File(f, "SystemFiles"+fileSeparator+"Links"+fileSeparator+"JLink"+fileSeparator+"JLink.jar");
        if (Files.exists(jLink.toPath())){
            return jLink;
        }

        jLink = new File(f, "10.0"+fileSeparator+"SystemFiles"+fileSeparator+"Links"+fileSeparator+"JLink"+fileSeparator+"JLink.jar");
        Log.log(Log.debugIndex,jLink.toString());
        if (Files.exists(jLink.toPath())){
            return jLink;
        }

        jLink = new File(f, "9.0"+fileSeparator+"SystemFiles"+fileSeparator+"Links"+fileSeparator+"JLink"+fileSeparator+"JLink.jar");
        Log.log(Log.debugIndex,jLink.toString());
        if (Files.exists(jLink.toPath())){
            return jLink;
        }

        jLink = new File(f, "8.0"+fileSeparator+"SystemFiles"+fileSeparator+"Links"+fileSeparator+"JLink"+fileSeparator+"JLink.jar");
        Log.log(Log.debugIndex,jLink.toString());
        if (Files.exists(jLink.toPath())){
            return jLink;
        }

        return null;
    }

    public File searchWindows(){
        File programFiles = new File(new File(System.getenv("ProgramFiles")), "Wolfram Research" + fileSeparator + "Mathematica");
        return isMathematicaFolder(programFiles);
    }

    public File searchLinux(){
        Map<String,String> map = System.getenv();
        map.forEach((s, s2) -> Log.log(Log.debugIndex,s,s2));
        File programFiles = new File("/usr/local/Wolfram/Mathematica");
        Log.log(Log.debugIndex,programFiles.getAbsolutePath());
        File jLink = isMathematicaFolder(programFiles);
        if (jLink==null){
            jLink = isMathematicaFolder(new File("/opt/Mathematica"));
            if (jLink==null){
                jLink = isMathematicaFolder(new File("/opt/Wolfram/Mathematica"));
            }
        }
        return jLink;
    }

    public File trySearch(){
        if (!SettingClass.MATHEMATICA_PATH.isEmpty()){
            File f = isMathematicaFolder(new File(SettingClass.MATHEMATICA_PATH));
            if (f!=null)
                return f;
        }
        String system = System.getProperty("os.name").toLowerCase();
        Log.log(Log.debugIndex,system);
        if (system.startsWith("l")){
            return searchLinux();
        } else {
            return searchWindows();
        }
    }
}
