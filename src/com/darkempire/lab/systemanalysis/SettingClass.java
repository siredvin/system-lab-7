package com.darkempire.lab.systemanalysis;

import com.darkempire.anji.log.Log;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by siredvin on 31.03.15.
 *
 * @author siredvin
 */
public class SettingClass {
    //Числові графічні налаштування
    public static double NODE_RADIUS = 20;
    public static double SMALL_NODE_RADIUS = 6;
    public static double GRAPH_PADDING = 14;
    public static double STROKE_WIDTH = 3;
    //Графічні налаштування кольору
    public static Color DEFAULT_NODE_FILL_COLOR = Color.WHITE;
    public static Color NODE_STROKE_COLOR = Color.BLACK;
    //Системні налаштуваня полів
    public static double MAX_DOUBLE_FIELD_WIDTH = 100;
    public static double SPINNER_STEP = 0.01;
    //Різні системні налаштування
    public static boolean GENERATE_MATRIX = true;
    //Різні системні константи (незмінні)
    public static String MATHEMATICA_PATH = "";

    private static final File settingPath = new File("setting.xml");

    public static void loadSetting(){
        Properties properties = new Properties();
        try {
            properties.loadFromXML(new FileInputStream(settingPath));
            NODE_RADIUS = Double.parseDouble(properties.getProperty("nodeRadius", String.valueOf(NODE_RADIUS)));
            SMALL_NODE_RADIUS = Double.parseDouble(properties.getProperty("smallNodeRadius", String.valueOf(SMALL_NODE_RADIUS)));
            GRAPH_PADDING = Double.parseDouble(properties.getProperty("graphPadding", String.valueOf(GRAPH_PADDING)));
            STROKE_WIDTH  = Double.parseDouble(properties.getProperty("strokeWidth", String.valueOf(STROKE_WIDTH)));

            MAX_DOUBLE_FIELD_WIDTH  = Double.parseDouble(properties.getProperty("maxDoubleFieldWidth", String.valueOf(MAX_DOUBLE_FIELD_WIDTH)));
            SPINNER_STEP = Double.parseDouble(properties.getProperty("spinnerStep", String.valueOf(SPINNER_STEP)));

            DEFAULT_NODE_FILL_COLOR = Color.valueOf(properties.getProperty("defaultNodeFillColor", DEFAULT_NODE_FILL_COLOR.toString()));
            NODE_STROKE_COLOR = Color.valueOf(properties.getProperty("nodeStrokeColor", NODE_STROKE_COLOR.toString()));

            GENERATE_MATRIX = Boolean.parseBoolean(properties.getProperty("generateMatrix", String.valueOf(GENERATE_MATRIX)));

            MATHEMATICA_PATH = properties.getProperty("mathematicaPath", MATHEMATICA_PATH);
        } catch (IOException e) {
            Log.error(Log.coreIndex, e);
        }
    }

    public static void saveSetting(){
        Properties properties = new Properties();
        properties.setProperty("nodeRadius", String.valueOf(NODE_RADIUS));
        properties.setProperty("smallNodeRadius", String.valueOf(SMALL_NODE_RADIUS));
        properties.setProperty("graphPadding", String.valueOf(GRAPH_PADDING));
        properties.setProperty("strokeWidth", String.valueOf(STROKE_WIDTH));

        properties.setProperty("maxDoubleFieldWidth", String.valueOf(MAX_DOUBLE_FIELD_WIDTH));
        properties.setProperty("spinnerStep", String.valueOf(SPINNER_STEP));

        properties.setProperty("defaultNodeFillColor", DEFAULT_NODE_FILL_COLOR.toString());
        properties.setProperty("nodeStrokeColor", NODE_STROKE_COLOR.toString());

        properties.setProperty("generateMatrix", String.valueOf(GENERATE_MATRIX));


        properties.setProperty("mathematicaPath", MATHEMATICA_PATH);
        try {
            properties.storeToXML(new FileOutputStream(settingPath),"System analysis Lab 7 setting file");
        } catch (IOException e) {
            Log.error(Log.coreIndex,e);
        }
    }



}
