package com.darkempire.lab.systemanalysis;

import com.darkempire.anji.log.Log;
import com.darkempire.math.struct.matrix.DoubleMatrix;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by siredvin on 04.04.15.
 *
 * @author siredvin
 */
public class CycleSearch {
    private static int size;
    public static Set<int[]>[] cycleHolder;
    public static int count = 0;
    public static int[] findEvenCycle(DoubleMatrix adjacencyMatrix){
        count = 0;
        size = adjacencyMatrix.getRowCount();
        if (cycleHolder!=null){
            for (Set<int[]> set:cycleHolder){
                set.clear();
            }
        }
        cycleHolder = new Set[size];
        for (int i=0;i<size;i++){
            cycleHolder[i] = new HashSet<>();
        }
        for (int i=0;i<size;i++){
            if (cycleHolder[i].isEmpty()) {
                int[] cycle = new int[size];
                Arrays.fill(cycle, -1);
                searchEvenCycle(adjacencyMatrix, cycle, i, 0);
            }
        }
        return null;
    }

    private static int[] searchEvenCycle(DoubleMatrix adjacencyMatrix,int[] prevStep,int index,int length){
        if (length==size)
            return null;
        prevStep[length] = index;
        for (int i=0;i<length;i++){
            if (prevStep[i]==index){
                if (isEvenCycle(prevStep,i,adjacencyMatrix,length+1)){
                    count++;
                    int[] cycle = new int[length-i+1];
                    System.arraycopy(prevStep,i,cycle,0,cycle.length);
                    cycleHolder[index].add(cycle);
                }
                return null;
            }
        }
        length++;
        for (int i=0;i<size;i++){
            double value = adjacencyMatrix.get(index,i);
            if (value!=0){
                int testIndex = i;
                if (cycleHolder[index].stream().filter(cycle->cycle[1]==testIndex).count()==0) {
                    searchEvenCycle(adjacencyMatrix, prevStep, i, length);
                    prevStep = prevStep.clone();
                }
            }
        }
        return null;
    }

    private static boolean isEvenCycle(int[] cycle,int startIndex,DoubleMatrix adjacencyMatrix,int length){
        double signHolder = 1;
        int prevStep = cycle[startIndex];
        for (int i=startIndex+1;i<length;i++){
            int nextStep = cycle[i];
            signHolder *= adjacencyMatrix.get(prevStep,nextStep);
            prevStep=nextStep;
        }
        return signHolder>0;
    }
}
