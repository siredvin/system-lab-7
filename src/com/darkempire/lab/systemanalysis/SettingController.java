package com.darkempire.lab.systemanalysis;

import com.darkempire.anji.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.stage.Stage;

import java.util.Date;

import static com.darkempire.lab.systemanalysis.SettingClass.*;

/**
 * Created by siredvin on 01.04.15.
 *
 * @author siredvin
 */
public class SettingController {

    @FXML
    private CheckBox matrixGenerateCheckBox;
    @FXML
    private Spinner<Double> nodeRadius;

    @FXML
    private Spinner<Double> pointerRadius;

    @FXML
    private Spinner<Double> graphPadding;

    @FXML
    private Spinner<Double> strokeWidth;

    @FXML
    private ColorPicker defaultNodeColor;

    @FXML
    private ColorPicker nodeStrokeColor;

    @FXML
    private Spinner<Double> spinnerMaxSize;

    @FXML
    private Spinner<Double> spinnerStep;

    private Stage stage;

    @FXML
    void initialize() {
        nodeRadius.setValueFactory(Main.createValueFactory(0.001,100,NODE_RADIUS,SPINNER_STEP));
        pointerRadius.setValueFactory(Main.createValueFactory(0.001,100,SMALL_NODE_RADIUS,SPINNER_STEP));
        graphPadding.setValueFactory(Main.createValueFactory(0.001,100,GRAPH_PADDING,SPINNER_STEP));
        strokeWidth.setValueFactory(Main.createValueFactory(0.001,100,STROKE_WIDTH,SPINNER_STEP));

        spinnerMaxSize.setValueFactory(Main.createValueFactory(0.001,100,MAX_DOUBLE_FIELD_WIDTH,SPINNER_STEP));
        spinnerStep.setValueFactory(Main.createValueFactory(1e-10, 100, SPINNER_STEP, 1e-10));
        //spinnerStep.queryAccessibleAttribute()

        defaultNodeColor.setValue(DEFAULT_NODE_FILL_COLOR);
        nodeStrokeColor.setValue(NODE_STROKE_COLOR);

        matrixGenerateCheckBox.setSelected(GENERATE_MATRIX);
    }

    public void init(Stage stage){
        this.stage=stage;
    }

    @FXML
    private void acceptAction(ActionEvent event) {
        NODE_RADIUS = nodeRadius.getValue();
        SMALL_NODE_RADIUS = pointerRadius.getValue();
        GRAPH_PADDING = graphPadding.getValue();
        STROKE_WIDTH = strokeWidth.getValue();

        MAX_DOUBLE_FIELD_WIDTH = spinnerMaxSize.getValue();
        SPINNER_STEP = spinnerStep.getValue();

        DEFAULT_NODE_FILL_COLOR = defaultNodeColor.getValue();
        NODE_STROKE_COLOR = nodeStrokeColor.getValue();

        GENERATE_MATRIX = matrixGenerateCheckBox.isSelected();

        SettingClass.saveSetting();
        stage.close();
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        stage.close();
    }
}
