package com.darkempire.lab.systemanalysis;

import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.anjifx.scene.DoubleField;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import com.darkempire.math.struct.matrix.Matrix;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.shape.Line;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;


import javax.swing.*;

import java.util.List;

import static com.darkempire.lab.systemanalysis.SettingClass.*;
/**
 * Created by siredvin on 31.03.15.
 *
 * @author siredvin
 */
public class MatrixEditController {
    @FXML
    private Label matrixSizeLabel;
    @FXML
    private GridPane matrixGrid;

    private Stage stage;
    private DoubleMatrix adjacencyMatrix;
    private Matrix<Spinner<Double>> matrixEditFields;
    private boolean accepted;

    @FXML
    void initialize() {
        accepted = false;
    }

    public void init(Stage stage,DoubleMatrix adjacencyMatrix,List<String> names){
        this.stage=stage;
        this.adjacencyMatrix = adjacencyMatrix;
        int size = adjacencyMatrix.getRowCount();
        matrixEditFields = Matrix.createInstance(size,size,new Spinner[size*size]);
        matrixEditFields.fill((rowIndex, columnIndex) -> {
            Spinner<Double> spinner = new Spinner<>();
            SpinnerValueFactory<Double> svf = Main.createValueFactory(-1,1,adjacencyMatrix.get(rowIndex,columnIndex),SPINNER_STEP);
            spinner.setValueFactory(svf);
            if (rowIndex==columnIndex){
                spinner.setOpacity(0.5);
                spinner.setFocusTraversable(false);
            } else {
                spinner.setEditable(true);
            }
            spinner.setMaxWidth(MAX_DOUBLE_FIELD_WIDTH);
            return spinner;
        });
        for (int i=0;i<size;i++){
            Label label = new Label(names.get(i));
            matrixGrid.add(label, 0, i + 1);
            GridPane.setHalignment(label, HPos.CENTER);
            label = new Label(names.get(i));
            matrixGrid.add(label, i + 1, 0);
            GridPane.setHalignment(label, HPos.CENTER);
        }
        for (int j=0;j<size;j++){
            for (int i=0;i<size;i++){
                matrixGrid.add(matrixEditFields.get(j,i),i+1,j+1);
            }
        }
    }

    @FXML
    private void acceptAction(ActionEvent event) {
        if (MonologGeneratorPane.showQuestionDialog("Ви певні?", "Ви точно хочете зберегти зміни?")) {
            adjacencyMatrix.fill((rowIndex, columnIndex) -> matrixEditFields.get(rowIndex, columnIndex).getValue());
            accepted = true;
            stage.close();
        }
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        if (MonologGeneratorPane.showQuestionDialog("Ви певні?","Ви точно не зберігати зміни?")) {
            stage.close();
        }
    }

    public boolean isAccepted(){
        return accepted;
    }
}
