package com.darkempire.lab.systemanalysis;

import com.darkempire.anji.document.wolfram.util.WolframConvertUtils;
import com.darkempire.anji.log.Log;
import com.darkempire.anji.util.Util;
import com.darkempire.anjifx.beans.property.AnjiColorProperty;
import com.darkempire.anjifx.beans.property.AnjiStringProperty;
import com.darkempire.anjifx.dialog.DialogUtil;
import com.darkempire.anjifx.monolog.MonologGeneratorPane;
import com.darkempire.math.MathMachine;
import com.darkempire.math.operator.matrix.DoubleMatrixTransformOperator;
import com.darkempire.math.struct.geometry.geomerty2d.Vector2D;
import com.darkempire.math.struct.matrix.DoubleArrayMatrix;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import com.darkempire.math.struct.vector.DoubleVector;
import com.wolfram.jlink.KernelLink;
import com.wolfram.jlink.MathLinkException;
import com.wolfram.jlink.MathLinkFactory;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static com.darkempire.lab.systemanalysis.SettingClass.*;

/**
 * Created by siredvin on 30.03.15.
 *
 * @author siredvin
 */
public class Controller {
    private static final int TEST_MATRIX_SIZE = 7;

    @FXML
    private VBox graphPane;
    @FXML
    private Pane graphView;

    private DoubleMatrix adjacencyMatrix;

    private KernelLink ml;
    private Stage stage;
    private ObservableList<String> nodeNames;
    private ObservableList<Color> nodeBackgroundColors;
    @FXML
    void initialize() {
        Random rand = new Random();
        nodeNames = FXCollections.observableArrayList();
        nodeBackgroundColors = FXCollections.observableArrayList();
        if (GENERATE_MATRIX) {
            adjacencyMatrix = DoubleArrayMatrix.resize(TEST_MATRIX_SIZE, TEST_MATRIX_SIZE);
            for (int i = 0; i < TEST_MATRIX_SIZE; i++) {
                nodeNames.add("S" + (i + 1));
                nodeBackgroundColors.add(DEFAULT_NODE_FILL_COLOR);
            }
            adjacencyMatrix.fill((rowIndex, columnIndex) -> {
                if (rowIndex == columnIndex)
                    return 0;
                if (rand.nextInt() % 2 == 0) {
                    return rand.nextDouble() - 0.5;
                } else {
                    return 0;
                }
            });
        } else {
            adjacencyMatrix = null;
        }
        drawGraph();
        ChangeListener<Number> resizeListener = (observable, oldValue, newValue) -> {
            graphView.getChildren().clear();
            drawGraph();
        };
        graphView.widthProperty().addListener(resizeListener);
        graphView.heightProperty().addListener(resizeListener);
        ml = null;
        try {
            ml = MathLinkFactory.createKernelLink("-linkmode launch -linkname 'math -mathlink'");
            ml.discardAnswer();
        } catch (MathLinkException e) {
            Log.error(Log.logIndex,e);
        }
    }

    public void init(Stage stage){
        this.stage=stage;
    }

    private void drawGraph(){
        //Обчислення радіусу
        if (adjacencyMatrix==null)
            return;
        graphView.getChildren().clear();
        double width = graphView.getWidth();
        double height = graphView.getHeight();
        double r = Math.min(width,height)/2;
        int count = adjacencyMatrix.getRowCount();
        List<Ellipse> ellipses = new ArrayList<>();
        List<Text> texts = new ArrayList<>();
        for (int i=0;i<count;i++){
            double centerX = r+(r-2*GRAPH_PADDING)*Math.cos(i * 2 * Math.PI / (double) count);
            double centerY = r+(r-2*GRAPH_PADDING)*Math.sin(i * 2 * Math.PI / (double) count);
            Ellipse ellipse =new Ellipse(centerX, centerY, NODE_RADIUS, NODE_RADIUS);
            ellipse.setFill(nodeBackgroundColors.get(i));
            ellipse.setStroke(NODE_STROKE_COLOR);
            ellipses.add(ellipse);
            Text text = new Text(centerX,centerY,nodeNames.get(i));
            texts.add(text);
            EllipseMouseListener onMouseClicked = new EllipseMouseListener(i,ellipse,text);
            ellipse.setOnMouseClicked(onMouseClicked);
            text.setOnMouseClicked(onMouseClicked);
        }
        graphView.getChildren().addAll(ellipses);
        graphView.getChildren().addAll(texts);
        for (int i=0;i<count;i++){
            for (int j=i+1;j<count;j++){
                double value = adjacencyMatrix.get(i,j);
                double reverseValue = adjacencyMatrix.get(j,i);
                if (value!=0 || reverseValue!=0){
                    Ellipse first = ellipses.get(i);
                    Ellipse second = ellipses.get(j);
                    Vector2D moveVector = new Vector2D(second.getCenterX()-first.getCenterX(),second.getCenterY()-first.getCenterY()).inormalize();
                    Vector2D lineStart = new Vector2D(first.getCenterX(),first.getCenterY()).ishift(moveVector, NODE_RADIUS);
                    Vector2D lineEnd = new Vector2D(second.getCenterX(), second.getCenterY()).ishift(moveVector,-NODE_RADIUS);
                    Shape line = new Line(lineStart.getX(),lineStart.getY(),lineEnd.getX(),lineEnd.getY());
                    if (reverseValue!=0){
                        line = Shape.union(line,new Ellipse(lineStart.getX(),lineStart.getY(),SMALL_NODE_RADIUS,SMALL_NODE_RADIUS));
                    }
                    if (value!=0){
                        line = Shape.union(line,new Ellipse(lineEnd.getX(),lineEnd.getY(),SMALL_NODE_RADIUS,SMALL_NODE_RADIUS));
                    }
                    if (value!=0 && reverseValue!=0){
                        if (value*reverseValue<0){
                            if (value>0){
                                line.setStyle("-fx-stroke:linear-gradient(green,red);-fx-stroke-width: "+STROKE_WIDTH);
                            } else {
                                line.setStyle("-fx-stroke:linear-gradient(red,green);-fx-stroke-width: "+STROKE_WIDTH);
                            }
                        } else {
                            oneColorFill(line,value);
                        }
                        Tooltip tooltip = new Tooltip("Від "+nodeNames.get(i)+" до "+nodeNames.get(j)+" залежність:"+ MathMachine.format(value)
                        +"\nВід " + nodeNames.get(j) +" до "+nodeNames.get(i)+" залежність:"+MathMachine.format(reverseValue));
                        tooltip.setWrapText(true);
                        Tooltip.install(line,tooltip);
                    } else {
                        String tooltipString;
                        if (reverseValue!=0){
                            tooltipString = "Від "+nodeNames.get(j)+" до "+nodeNames.get(i)+" залежність:"+ MathMachine.format(reverseValue);
                            oneColorFill(line,reverseValue);
                        } else {
                            tooltipString = "Від "+nodeNames.get(i)+" до "+nodeNames.get(j)+" залежність:"+ MathMachine.format(value);
                            oneColorFill(line,value);
                        }
                        Tooltip tooltip = new Tooltip(tooltipString);
                        tooltip.setWrapText(true);
                        Tooltip.install(line, tooltip);
                    }
                    graphView.getChildren().add(line);
                }
            }
        }
        texts.forEach(text -> {
            Bounds bounds = text.getBoundsInParent();
            text.setX(text.getX() - bounds.getWidth() / 2);
            text.setY(text.getY() + bounds.getHeight() / 4);
        });
    }

    @FXML
    private void checkEvenAction(ActionEvent event) {
        if(adjacencyMatrix==null) {
            return;
        }
        int size = adjacencyMatrix.getRowCount();
        CycleSearch.findEvenCycle(adjacencyMatrix);
        Log.log(Log.debugIndex, CycleSearch.count);
        if (CycleSearch.count>0){
            StringBuilder builder = new StringBuilder();
            for (Set<int[]> vertexSet:CycleSearch.cycleHolder){
                for (int[] cycle:vertexSet){
                    for (int aCycle : cycle) {
                        builder.append(nodeNames.get(aCycle)).append("->");
                    }
                    builder = Util.removeLastChars(builder,2).append('\n');
                }
            }
            MonologGeneratorPane.showDetailInfoDialog("Система не стійка структурно", "У системі наявні парні цикли:" + CycleSearch.count+" штук", builder.toString());
        } else {
            MonologGeneratorPane.showAcceptDialog("Системна стійка структурно","У системи відсутні парні цикли");
        }
    }


    private void oneColorFill(Shape shape,double value){
        if (value>0){
            shape.setStyle("-fx-stroke: green;-fx-stroke-width:" + STROKE_WIDTH);
        } else {
            shape.setStyle("-fx-stroke: red;-fx-stroke-width: " + STROKE_WIDTH);
        }
    }

    @FXML
    private void eigenValuesAction(ActionEvent event) {
        if(adjacencyMatrix==null)
            return;
        String result = ml.evaluateToInputForm("N[Abs /@ Eigenvalues["+adjacencyMatrix.toWolframInput()+"]]",0);
        DoubleVector eigenNumberVector = WolframConvertUtils.convertResultToDoubleFixedVector(result);
        boolean presentEqualOne = false;
        boolean presentBiggerOne = false;
        int size = eigenNumberVector.getSize();
        for (int i=0;i<size;i++){
            double el = eigenNumberVector.get(i);
            if (el>1){
                presentBiggerOne = true;
            }
            if (el==1){
                presentEqualOne = true;
            }
        }
        StringBuilder builder = new StringBuilder();
        if (presentBiggerOne){
            builder.append("Система не стійка за збуреннями");
        } else {
            builder.append("Система стійка за збуреннями");
            builder.append(" та ");
            if (presentEqualOne){
                builder.append("не стійка за значенням");
            } else {
                builder.append("стійка за значенням");
            }
        }
        MonologGeneratorPane.showInfoDialog(builder.toString(),"Модулі власних чисел:"+result);
    }

    @FXML
    private void showMatrixAction(ActionEvent event) {
        if(adjacencyMatrix==null)
            return;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controller.class.getResource("res/matrixEdit.fxml"));
        Stage stage = new Stage();
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            Log.error(Log.logIndex, e);
        }
        stage.initOwner(this.stage);
        MatrixEditController controller  = loader.getController();
        controller.init(stage, adjacencyMatrix, nodeNames);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        if (controller.isAccepted()){
            drawGraph();
        }
    }

    @FXML
    private void loadMatrixAction(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Matrix saved values", "*.msv"));
        File f = chooser.showOpenDialog(stage);
        if (f!=null){
            try {
                Scanner in = new Scanner(new FileInputStream(f));
                List<String> strings = new ArrayList<>();
                while (in.hasNextLine()){
                    strings.add(in.nextLine());
                }
                in.close();
                String firstLine = strings.get(0);
                String[] firstRowValues = firstLine.split(";");
                int size = firstRowValues.length;
                adjacencyMatrix = DoubleArrayMatrix.resize(size,size);
                adjacencyMatrix.fillFirstRow((rowIndex, columnIndex) -> MathMachine.parse(firstRowValues[columnIndex]).doubleValue());
                for (int rowIndex=1;rowIndex<size;rowIndex++){
                    String[] values = strings.get(rowIndex).split(";");
                    adjacencyMatrix.fillRow(rowIndex, (rowIndex1, columnIndex) -> MathMachine.parse(values[columnIndex]).doubleValue());
                }
                nodeNames = FXCollections.observableList(strings.subList(size,2*size));
                nodeBackgroundColors = FXCollections.observableList(strings.subList(2*size,3*size).stream()
                .map(Color::valueOf).collect(Collectors.toList()));
                drawGraph();
            } catch (FileNotFoundException e) {
                Log.error(Log.logIndex,e);
            }
        }
    }

    @FXML
    private void addNodeAction(ActionEvent event) {
        String result = DialogUtil.createStringDialog("Ім’я вузла");
        if (!result.isEmpty()){
            int size = nodeNames.size();
            nodeNames.add(result);
            nodeBackgroundColors.add(DEFAULT_NODE_FILL_COLOR);
            DoubleMatrix newMatrix = DoubleArrayMatrix.resize(size+1,size+1);
            newMatrix.fill((rowIndex, columnIndex) -> {
                if (rowIndex<size && columnIndex<size){
                    return adjacencyMatrix.get(rowIndex,columnIndex);
                }
                return 0;
            });
            adjacencyMatrix = newMatrix;
            drawGraph();
        }
    }

    @FXML
    private void removeNodeAction(ActionEvent event) {
        if(adjacencyMatrix==null)
            return;
        String result = DialogUtil.selectDialog("Ім’я вузла", nodeNames);
        if (!result.isEmpty()){
            if (MonologGeneratorPane.showQuestionDialog("Ви точно хочете видалити вузол "+result)){
                int index = nodeNames.indexOf(result);
                nodeNames.remove(result);
                nodeBackgroundColors.remove(index);
                adjacencyMatrix = DoubleMatrixTransformOperator.removeColumn(DoubleMatrixTransformOperator.removeRow(adjacencyMatrix,index),index);
                drawGraph();
            }
        }
    }

    @FXML
    private void saveMatrixAction(ActionEvent event) {
        if(adjacencyMatrix==null)
            return;
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Matrix saved values","*.msv"));
        File f = chooser.showSaveDialog(stage);
        if (f!=null){
            try {
                if (!f.getAbsolutePath().contains(".msv")){
                    f = new File(f.getAbsolutePath()+".msv");
                }
                String matrix = adjacencyMatrix.toString().replace('\t',';');
                PrintWriter pw = new PrintWriter(new FileOutputStream(f));
                pw.print(matrix);
                nodeNames.forEach(pw::println);
                nodeBackgroundColors.forEach(pw::println);
                pw.close();
            } catch (FileNotFoundException e) {
                Log.error(Log.logIndex,e);
            }
        }
    }

    @FXML
    private void settingAction(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controller.class.getResource("res/setting.fxml"));
        Stage stage = new Stage();
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            Log.error(Log.logIndex,e);
        }
        stage.setTitle("Налаштування");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(this.stage);
        SettingController controller = loader.getController();
        controller.init(stage);
        stage.showAndWait();
        drawGraph();
    }


    private class EllipseMouseListener implements EventHandler<MouseEvent> {
        private int index;
        private Ellipse ellipse;
        private Text text;
        public EllipseMouseListener(int index,Ellipse ellipse,Text text){
            this.index=index;
            this.ellipse=ellipse;
            this.text=text;
        }

        @Override
        public void handle(MouseEvent event) {
            if (event.getButton()== MouseButton.SECONDARY) {
                Bounds oldBound = text.getBoundsInParent();
                AnjiColorProperty colorProperty = new AnjiColorProperty(ellipse,"Колір фону",nodeBackgroundColors.get(index));
                AnjiStringProperty nameProperty = new AnjiStringProperty(ellipse,"Ім’я",text.getText());
                DialogUtil.createDialog("Властивості вузла",nameProperty,colorProperty).showAndWait();
                nodeBackgroundColors.set(index, colorProperty.getValue());
                nodeNames.set(index,nameProperty.getValue());
                ellipse.setFill(colorProperty.getValue());
                text.setText(nameProperty.getValue());
                Bounds bounds = text.getBoundsInParent();
                text.setX(text.getX() - bounds.getWidth() / 2 + oldBound.getWidth()/2);
                text.setY(text.getY() + bounds.getHeight() / 4 - oldBound.getHeight()/4);
            }
        }
    }
}
